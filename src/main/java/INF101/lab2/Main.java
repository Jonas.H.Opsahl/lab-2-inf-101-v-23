package INF101.lab2;

import INF101.lab2.pokemon.IPokemon;
import INF101.lab2.pokemon.Pokemon;

public class Main {

    public static IPokemon pokemon1;
    public static IPokemon pokemon2;
    public static void main(String[] args) {
        pokemon1 = new Pokemon("Pikachu");
        pokemon2 = new Pokemon("Oddish");
        while (pokemon1.isAlive() == true || pokemon2.isAlive() == true) {
            pokemon1.attack(pokemon2);
            pokemon2.attack(pokemon1);
        }
    }
}
